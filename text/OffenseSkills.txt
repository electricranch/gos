Skill	Mobility	Awareness	Presence	Intellect	Toughness	Versus Mobility	Versus Awareness	Versus Presence	Versus Intellect	Versus Toughness	Bohan	Chott	Human	Jeyo	Kamoan	Mech	Nalamar	Radoni	Tilak	Woro	Assassin	Brawler	Commander	Disciple	Genius	Hunter	Mechanic	Noble	Oracle	Professional	Raider	Saboteur	Trickster	Vanguard	Warrior	Damage	Persistent	Exertion	Move	Jump	Push	Pull	Slide	Range	Area Burst	Area Blast	Status Bleed	Status Blind	Status Break	Status Confused	Status Dazed	Status Disarmed	Status Drained	Status Fatigued	Status Fear	Status Pain	Status Pin	Status Poisoned	Status Prone	Status Sickened	Status Slowed	Element	Requirements
Slam	1	0	0	0	1	1	0	0	0	1	2	4	2	1	1	3	1	3	2	4	1	4	0	2	0	1	0	0	0	0	2	0	0	3	2	3	0	-1	1	0	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	blu	Compatible
Disarm	1	0	0	0	1	1	0	0	0	1	2	0	0	2	0	1	0	1	2	3	3	3	0	2	0	1	0	0	0	0	3	2	2	2	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	0	0	0	0	0	0	0	0	blu	Hands:1
Grapple	0	0	0	0	2	1	0	0	0	1	1	4	1	1	1	3	1	2	2	4	0	4	0	0	0	0	0	0	0	0	2	0	0	3	2	1	0	-2	-2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	2	0	2	blu	Hands:2
Trip	1	0	0	0	1	2	0	0	0	0	2	0	0	2	0	1	0	0	4	2	3	2	0	3	0	0	0	0	0	0	0	0	0	2	0	1	0	0	-1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	0	blu	Compatible
Bum Rush	0	0	0	0	2	0	0	0	0	2	0	1	1	0	0	2	0	2	0	4	0	4	0	0	0	0	0	0	0	0	2	0	0	4	2	1	0	-2	2	0	3	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	blu	Compatible
Shove	0	0	0	0	2	0	0	0	0	2	0	3	0	0	0	4	0	2	0	4	0	4	0	1	0	1	0	0	0	0	3	0	1	3	2	1	0	-2	0	0	0	0	4	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	1	0	0	blu	Hands:1
Claws	2	0	0	0	0	1	0	0	0	1	1	0	0	2	0	0	0	1	1	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	sla	Hands:1
Bite	0	0	0	0	2	0	0	0	0	2	0	3	0	0	0	0	0	3	1	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	0	-1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	imp	Compatible
Appendage Whip	2	0	0	0	0	2	0	0	0	0	0	0	0	1	0	0	0	0	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	-1	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	0	0	blu	Compatible
