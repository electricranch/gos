package net.myexperiments.gos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GOS {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// System Configuration
				GOSSystem.getLogger().setLevel(Level.ALL);
				boolean append = true;
			    FileHandler handler = new FileHandler("gos.log", append);
			    GOSSystem.getLogger().addHandler(handler);
			    GOSSystem.getLogger().addHandler(new ConsoleHandler());
			    	
		
/**
 * 
 * 		Setup table names
 * 
 * 
 */
		
		Game TheGame = new Game();
		TheGame.SystemConfig = new Properties();
		TheGame.SystemConfig.load(new FileInputStream(GOSSystem.CONFIGDIRECTORY
				+ File.separatorChar + "SysConfig.properties"));


		TheGame.InitGame();
/**
 * 
 * 		Go to EventLoop
 * 
 */
		TheGame.Start();
		
	}

}
