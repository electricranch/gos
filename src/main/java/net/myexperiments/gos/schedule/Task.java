package net.myexperiments.gos.schedule;

import net.myexperiments.gos.GOSSystem;
import net.myexperiments.gos.schedule.Sleepable.STATES;

public class Task implements Comparable<Task>{

//	long WakeupTime;
	long SleepTime;
	Sleepable OuterClass;
	public STATES STATES;
	Integer turn;
    public Task( Sleepable OuterClass) {
    	this.OuterClass = OuterClass;
    }
	
    public int compareTo(Task t) {
    	if (this.SleepTime < t.SleepTime) return -1;
    	if (this.SleepTime > t.SleepTime) return 1;
    	return 0;
    }
    
	public void TurnBasedSleep (Integer Turn, Sleepable.STATES STATES) {
		GOSSystem.getLogger().entering("Turn based sleeping by " + OuterClass.getName(), "Turn " + Turn.toString());
		this.STATES = STATES;
		OuterClass.getWorld().scheduler.TurnBasedSleep(Turn,this);
	}

	public void RealTimeSleep (long Sleeptime, Sleepable.STATES STATES) {
		GOSSystem.getLogger().entering("RealTime Sleeping  by " + OuterClass.getName(), " extra ");

		this.STATES = STATES;
		this.SleepTime = Sleeptime;
		OuterClass.getWorld().scheduler.RealTimeSleep(this);
	}
	
	public long getSleepTime() {
		return SleepTime;
	}

	public void setSleepTime(long sleepTime) {
		SleepTime = sleepTime;
	}


	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public long getSleeptine() {
		// TODO Auto-generated method stub
		return SleepTime;
	}

	public Object[] getSTATES() {
		// TODO Auto-generated method stub
		return null;
	}

}
