package net.myexperiments.gos.schedule;

import net.myexperiments.gos.World;


public interface Sleepable {
	
	public enum STATES {
		BORN, LIVING, HUNGRY, DEATH, FIGHTING, EXPLORING, EXPLAINING;
	}	
	World getWorld();
	long getSleeptime();
	void Wakeup(Task t);
	String getName();;
	
}
