package net.myexperiments.gos.schedule;

import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

import net.myexperiments.gos.World;

public class EventQueue {
	public long time = 0;
	public long lastTime = 0;
	PriorityQueue<Task> CurrentTurnQueue;


	World world;

	public EventQueue(World map) {
		world = map;
		world.TurnQueue = new HashMap<Integer, PriorityQueue<Task>>();
	};
	
	public void RealTimeSleep(Task e) {
		world.realtime.add(e);
	}
	public void TurnBasedSleep(Integer turn, Task e) {
		
		if(world.TurnQueue.get(turn) == null ) world.TurnQueue.put(turn, new PriorityQueue<Task>());
		world.TurnQueue.get(turn).add(e);
	}
	
	public void EventLoop() {
		
		// Process Realtime Queue
		time = System.currentTimeMillis();
		Iterator<Task> itr = world.realtime.iterator();
		while (itr.hasNext()) {
			Task CurrentTask = itr.next();
			if (time > CurrentTask.OuterClass.getSleeptime() ) {
				CurrentTask.OuterClass.Wakeup(CurrentTask);
				itr.remove();
			}
		}
		// Process turn based queue
		if ((  CurrentTurnQueue  = world.TurnQueue.get(world.CurrentTurn)) != null) { 
		while (!CurrentTurnQueue.isEmpty()) {
			Task CurrentTask = CurrentTurnQueue.poll();
					CurrentTask.OuterClass.Wakeup(CurrentTask);
					System.out.println("Name :" + CurrentTask.OuterClass.getName());
		}
		world.TurnQueue.remove(CurrentTurnQueue); // assure GC
		}
		world.CurrentTurn++;
		
			
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
}
