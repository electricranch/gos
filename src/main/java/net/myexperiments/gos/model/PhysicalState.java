/**
 * 
 */
package net.myexperiments.gos.model;

/**
 * @author bruce
 *
 */
public enum PhysicalState {
	LIGHT,HEAVY,WET,MOBILE,IMOBILE,DAMAGED,NEW;

}
