package net.myexperiments.gos.model;

import java.util.HashMap;

import net.myexperiments.gos.World;
import net.myexperiments.gos.agent.npc;
import net.myexperiments.gos.pathfinder.Node;
import net.myexperiments.gos.schedule.Sleepable;
import net.myexperiments.gos.schedule.Task;

public class Room extends Location implements Sleepable {
	HashMap<String, InanimateObject> intheRoom = new HashMap<String, InanimateObject>();
	HashMap<String, npc> people = new HashMap<String, npc>();
	World world;
	Node room;
	Task task;
	
	public long getSleeptime() {
		return task.getSleepTime();
	}
	
	public World getWorld(){
		return world;
	}
	
	public Room(World world) {
		this.world = world;
		room = new Node();
		task = new Task(this);
	}

	public void Wakeup(Task t) {
			
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "A Room";
	};

}
