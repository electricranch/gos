package net.myexperiments.gos.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import net.myexperiments.gos.GOSSystem;
import net.myexperiments.gos.World;
import net.myexperiments.gos.model.InanimateObject;
import net.myexperiments.gos.pathfinder.Node;

import net.myexperiments.gos.schedule.Sleepable;
import net.myexperiments.gos.schedule.Task;
import net.myexperiments.gos.messaging.*;

public class GameCharacter extends AnimateObject implements Sleepable, MailUser {

	HashMap<String, Object> characteristics = new HashMap<String, Object>();
	ArrayList<InanimateObject> possessions = new ArrayList<InanimateObject>();
	Task task;
	World world;
	Node loc;
	String name;
	String CharacterClass;
	String Species;
	Logger CharacterLog;
	
	public GameCharacter(World map, String name) {
		world = map;
		this.name = name;
		task = new Task(this);
		task.TurnBasedSleep(world.CurrentTurn + 1, STATES.BORN);
		CharacterLog = GOSSystem.getLogger();
		world.GOSPS.registerUser(name, this);
	}


	public void Wakeup(Task t) {
			CharacterLog.entering("GameCharacter " + name, "Wakeup", task.getSTATES());
		
		switch (t.STATES) {
		
		case BORN:
			CharacterLog.entering("GameCharacter " + name, "BORN");
			// Sleep one second and wake up hungry
			task.RealTimeSleep(1000, STATES.HUNGRY);		
			break;
		case HUNGRY:
			CharacterLog.entering("GameCharacter " + name, "HUNGRY");
			// Start exploring next turn
			task.TurnBasedSleep(world.CurrentTurn + 1, STATES.EXPLORING);
			break;
		case DEATH:
			CharacterLog.entering("GameCharacter " + name, "DEAD");
			break;
		case EXPLORING:
			CharacterLog.entering("GameCharacter " + name, "EXPLORING");
			task.RealTimeSleep(2000, STATES.LIVING);
			break;
		case FIGHTING:
			CharacterLog.entering("GameCharacter " + name, "FIGHTING");
			// Fight lasts 3 seconds
			task.RealTimeSleep(3000, STATES.LIVING);
			break;	
		case LIVING:
			CharacterLog.entering("GameCharacter " + name, "LIVING");
			task.TurnBasedSleep(world.CurrentTurn+2, STATES.LIVING);
			break;
		}

	}



	// methods needed to implement MailUser

	public void ReceiveMail(Message letter) {

		switch (letter.getType()) {
		
		case BITEME:
			CharacterLog.entering(" GameCharacter ", " ReceiveMail ", "From " + letter.getSender() + " To " + name );
			break;
				
		}
		
	}

	// methods needed to implement Sleepable
	
	public long getSleeptime() {
		return task.getSleeptine();
	}

	public World getWorld() {
		return world;
	}
	
	// Getters and Setters
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCharacterClass() {
		return CharacterClass;
	}
	public void setCharacterClass(String class1) {
		CharacterClass = class1;
	}
	public String getSpecies() {
		return Species;
	}
	public void setSpecies(String species) {
		Species = species;
	}

};
