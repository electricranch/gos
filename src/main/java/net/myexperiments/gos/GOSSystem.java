package net.myexperiments.gos;

import java.io.File;
import java.util.logging.Logger;



public class GOSSystem {
	private final static Logger LOGGER = Logger.getLogger(GOSSystem.class.getName());
	// Needs special location to load from jar - work out later
	public static final String TABLEDIRECTORY = "text"+ File.separatorChar;
	public static final String CONFIGDIRECTORY = "config"+ File.separatorChar;
	public static final String RULESDIRECTORY = "rules"+ File.separatorChar;

	public static Logger getLogger() {
		return LOGGER;
	}
}
