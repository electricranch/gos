package net.myexperiments.gos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import net.myexperiments.gos.model.WorldObjects;
import net.myexperiments.gos.tableClasses.Characters;
import net.myexperiments.gos.tableClasses.Classes;
import net.myexperiments.gos.tableClasses.Doors;
import net.myexperiments.gos.tableClasses.NameVector;
import net.myexperiments.gos.tableClasses.Rooms;
import net.myexperiments.gos.tableClasses.Species;

public class CreateWorld {
	public enum ClassNames {
		Doors, Characters, Rooms, Classes, Species, WorldObjects, Names, MagicText, StafforWand, Sword, Rings, SecondaryItems, MagicItem 
	}
	World world;

	CreateWorld(World world) throws FileNotFoundException, IOException {
		
		this.world = world;
		//	load property file with table file name
		Properties tables = new Properties();
		String TableFile = world.SystemConfig.getProperty("TableConfigFile");
//		tables.load(new FileInputStream(world.CONFIGDIRECTORY + File.separatorChar + TableFile));
		tables.load(new FileInputStream(GOSSystem.CONFIGDIRECTORY + File.separatorChar + TableFile));
			
/**
 * 
 * 
 * 		Load tables into classes
 * 
 * 
 */
		
		Set<String> items = tables.stringPropertyNames();
		Iterator<String> iter = items.iterator();
		while (iter.hasNext()) {
			String itemName = iter.next();
			String filename = tables.getProperty(itemName);
			try {
				BuildClass(itemName, GOSSystem.TABLEDIRECTORY + filename);
//				BuildClass(itemName, filename);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}


	private void BuildClass(String item, String filename) throws IOException {

		switch (ClassNames.valueOf(item)) {

		case Doors:
			world.doors  = new Doors(filename, world);
			break;
			
		case Characters:
			world.characters = new Characters(filename,world);
			break;
			
		case Rooms:
			world.rooms = new Rooms(filename,world);
			break;
			
		case Classes:
			world.classes = new Classes(filename,world);
			break;
			
		case Species:
			world.species = new Species(filename,world);
			break;
			
		case WorldObjects:
			world.worldobjects = new WorldObjects(filename,world);
			break;
			
		case Names:
			world.names = new NameVector(filename, world);
			break;

		}
		
		

	}

}
