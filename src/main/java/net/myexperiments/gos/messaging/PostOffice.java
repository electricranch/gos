package net.myexperiments.gos.messaging;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class PostOffice {
	HashMap<String, Vector<Message>> MailBoxes;
	HashMap<String, MailUser> Customers;
	
	public void registerUser(String user, MailUser userClass) {
		MailBoxes.put(user, new Vector<Message>());
		Customers.put(user, userClass);
	}

	public PostOffice() {
		MailBoxes = new HashMap<String, Vector<Message>>();
		Customers = new HashMap<String, MailUser>();
	}

	public Message ReceiveMessage(String user) {
		Vector<Message> yourBox = MailBoxes.get(user);
		if ( !yourBox.isEmpty()) {
		Message letter = yourBox.firstElement();
		yourBox.remove(yourBox.indexOf(letter));
		return letter;
		}
		return null; 
	}

	public boolean SendMessage(String recipent, Message letter) {
		if ( !Customers.containsKey(recipent)) return false;
		Vector<Message> senderBox = MailBoxes.get(recipent);
		senderBox.add(letter);
		return true;
	}
	
	public void SendMail() {
			Message msg;
			Iterator<String> itr =  Customers.keySet().iterator();
			while ( itr.hasNext()) {
			String user = itr.next();
			while  ((msg = ReceiveMessage(user)) != null ) {
				Customers.get(user).ReceiveMail(msg);
			}
		}
		
	}
}
