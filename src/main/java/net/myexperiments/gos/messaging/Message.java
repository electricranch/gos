package net.myexperiments.gos.messaging;

public class Message {
String sender;
MessageType type;
String message;
	public MessageType getType() {
	return type;
}
public void setType(MessageType type) {
	this.type = type;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public void setSender(String sender) {
	this.sender = sender;
}
	public String getSender() {
	return sender;
}
	Message( String Sender, MessageType Type, String Message ) {
		sender = Sender;
		type = Type;
		message = Message;
	}
}
