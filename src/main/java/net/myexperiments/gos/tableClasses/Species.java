package net.myexperiments.gos.tableClasses;

import java.io.IOException;
import java.util.Set;

import net.myexperiments.gos.World;

public class Species extends Loadable {

	public Species(String filename, World world) throws IOException {
		super(filename, world);
		world.species = this;
		// TODO Auto-generated constructor stub
	}
	String getRandomSpecies() {
		Set<String> SpeciesNames = super.objectsloaded.keySet();
		int index = ((int)(Math.random() * ((SpeciesNames.size() + 1))));
		return (String) SpeciesNames.toArray()[index];
	}
}
