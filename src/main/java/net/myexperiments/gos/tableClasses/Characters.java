package net.myexperiments.gos.tableClasses;

import java.io.IOException;

import net.myexperiments.gos.*;
import net.myexperiments.gos.agent.GameCharacter;
public class Characters extends Loadable {
	
	public Characters(String characterdata, World world) throws IOException {
		super(characterdata,world);
		// TODO Auto-generated constructor stub
	}

	public GameCharacter CreateRandomCharacter(){
		String theName = world.names.getRandomName();
		String theSpecies = world.species.getRandomSpecies();
		String theClass = world.classes.getRandomeClass();
		
		return CreateCharacter(theName, theSpecies, theClass);
		
	}
	
	public GameCharacter CreateCharacter( String theName, String theSpecies, String theClass) {
		GameCharacter theCharacter = new GameCharacter(world, theName);
		theCharacter.setCharacterClass(theClass);
		theCharacter.setSpecies(theSpecies);
		return theCharacter;
		
	}
	
}
