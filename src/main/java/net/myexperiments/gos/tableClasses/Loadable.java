package net.myexperiments.gos.tableClasses;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.myexperiments.gos.GOSSystem;
import net.myexperiments.gos.World;

import org.jumpmind.symmetric.csv.*;

public class Loadable {
	World world;
	/**
	 * 
	 * fields are the column names, objects loaded is the table, HashMap row and
	 * col for easy quick lookup
	 * 
	 */
	String[] fields;
	HashMap<String, HashMap<String, String>> objectsloaded = new HashMap<String, HashMap<String, String>>();

	protected Loadable(String filename, World world) throws IOException {
		setWorld(world);
//		GOSSystem.getLogger().entering("Loadable loading:", filename);
		CsvReader reader = new CsvReader(new FileReader(filename), '\t');
		if (!reader.readHeaders()) {
//			GOSSystem.getLogger().exiting("Loadable", "readHeaders",
//					"Cannot read header of " + filename);
			throw new IOException("Cannot read header of " + filename);
		}
		fields = reader.getHeaders();
		while (reader.readRecord()) {
			HashMap<String, String> rowdata = new HashMap<String, String>();
			for (int i = 0; i < fields.length; i++) {
				rowdata.put(fields[i], reader.get(fields[i]));
			}
			objectsloaded.put(reader.get(fields[0]), rowdata);
		}

		if (world.getDumpdata())
			DumpData(filename);
		GOSSystem.getLogger().exiting("Loadable", "readHeaders",
				"Read header of " + filename);
	}

	public void DumpData(String filename) {
		System.out.println("Dumping Fields from " + filename);
		for (int i = 0; i < fields.length; i++) {
			System.out.print(" " + fields[i]);
		}
		System.out.println("Dumping Field Data");
		System.out.println(objectsloaded.toString());
	}

	/**
	 * 
	 * Getters and Setters
	 * 
	 * @return
	 */
	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public HashMap<String, HashMap<String, String>> getObjectsloaded() {
		return objectsloaded;
	}

	public void setObjectsloaded(
			HashMap<String, HashMap<String, String>> objectsloaded) {
		this.objectsloaded = objectsloaded;
	}

}
