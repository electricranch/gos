package net.myexperiments.gos.tableClasses;

import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import net.myexperiments.gos.World;

import org.jumpmind.symmetric.csv.CsvReader;


public class NameVector {;
World world;

	public Vector<String> NameVector = new Vector<String>();

	public NameVector(String filename, World world) throws IOException {
		CsvReader reader = new CsvReader(new FileReader(filename), '\t');
		
		while (reader.readRecord()) {
			String[] thisRow = reader.getValues();
			for (int i = 0; i < thisRow.length; i++) {;
				addName(thisRow[i]);
			}
		}
	}
	
	public String getRandomName(){
		int Max = NameVector.size();
		return getName ((int)(Math.random() * ((Max + 1))));
	}
	
	String getName(int i) {
		String Name = NameVector.get(i);
		NameVector.remove(i);
		return Name;
		
	}
	public void addName(String Name) {
	NameVector.add(Name);
	}
}
