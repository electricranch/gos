package net.myexperiments.gos;



import java.io.IOException;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Properties;

import net.myexperiments.gos.messaging.PostOffice;
import net.myexperiments.gos.model.WorldObjects;
import net.myexperiments.gos.schedule.EventQueue;
import net.myexperiments.gos.schedule.Task;
import net.myexperiments.gos.tableClasses.Characters;
import net.myexperiments.gos.tableClasses.Classes;
import net.myexperiments.gos.tableClasses.Doors;
import net.myexperiments.gos.tableClasses.NameVector;
import net.myexperiments.gos.tableClasses.Rooms;
import net.myexperiments.gos.tableClasses.Species;


public class World {

	public Properties SystemConfig; // Master Properties file

	// Development flags
	public boolean dumpdata = false;
	public boolean debug = false;

	// sub-systems objects
	public CreateWorld layout;
	public EventQueue scheduler;


	// Items in model
	public Doors doors;
	public Characters characters;
	public Rooms rooms;

	// data table objects
	public Classes classes;
	public Species species;
	public WorldObjects worldobjects;
	public NameVector names;
	public PostOffice GOSPS;
	
	// MultiTasker 
	public PriorityQueue<Task> realtime = new PriorityQueue<Task>();  // Realtime processing
	// Turn Based processing
	public HashMap<Integer, PriorityQueue<Task>> TurnQueue = new HashMap<Integer,PriorityQueue<Task>>();
	public Integer CurrentTurn = 0;
	
	public World() throws IOException {
	}

	public void InitWorld() throws IOException {
		
		/**
		 * 
		 * Setup System
		 * 
		 */
		layout = new CreateWorld(this);
		scheduler = new EventQueue(this);
		GOSPS = new PostOffice();
		/**
		 * 
		 * Create console
		 * 
		 */

		/**
		 * 
		 * Support for characters
		 * 
		 */
		// GameCharacter test = new GameCharacter( this, this.names.getRandomName());
		/**
		 * 
		 * Setup Logging
		 * 
		 * 
		 */

		/**
		 * 
		 * Build the World from the tables read in
		 * 
		 * Build rooms and npc and rpc from table data read in
		 * 
		 * Setup processes in the event system
		 * 
		 * 
		 */

	};

	/**
	 * 
	 * And we're off!
	 * 
	 * 
	 */

	void Start() {
		String echo = "Hello World";
		for (;;) {
			GOSPS.SendMail();
			scheduler.EventLoop();

		}

	}

	/**
	 * 
	 * 
	 * Getters and Setters
	 * 
	 * @return
	 */
	public boolean getDumpdata() {
		return dumpdata;
	}

}
